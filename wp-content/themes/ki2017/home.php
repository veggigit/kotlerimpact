<!-- Header -->
<?php get_header(); ?>
<!-- Banner Video -->
<?php get_template_part( 'assets/include/content', 'home_video' ); ?>
<!-- About -->
<?php get_template_part( 'assets/include/content', 'home_about' ); ?>
<!-- Program -->
<?php get_template_part( 'assets/include/content', 'home_program' ); ?>
<!-- Profesores destacados -->
<?php get_template_part( 'assets/include/content', 'home_profesores' ); ?>
<!-- Features -->
<?php get_template_part( 'assets/include/content', 'home_features' ); ?>
<!-- Footer -->
<?php get_footer ();?>
