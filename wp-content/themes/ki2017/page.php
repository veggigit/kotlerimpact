<?php get_header(); ?>
<?php
if(is_page(2)){
  get_template_part( 'assets/include/content', 'nosotros' );
}
elseif(is_page(11)){
  get_template_part( 'assets/include/content', 'sobre' );
}
elseif(is_page(13)){
  get_template_part( 'assets/include/content', 'programa' );
}
elseif(is_page(15)){
  get_template_part( 'assets/include/content', 'profesores' );
}
elseif(is_page(17)){
  get_template_part( 'assets/include/content', 'prensa' );
}
elseif(is_page(25)){
  get_template_part( 'assets/include/content', 'revista' );
}
elseif(is_page(19)){
  get_template_part( 'assets/include/content', 'adquirir' );
}
elseif(is_page(29)){
  get_template_part( 'assets/include/content', 'base_adquirir' );
}else{
  get_template_part( 'assets/include/content', 'default' );
}
?>
<?php get_footer ();?>
