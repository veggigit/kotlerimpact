<?php
 ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <?php wp_head(); ?>

	<meta charset="utf-8">
	<title><?php bloginfo ('name'); ?></title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta name="description" content="Kotler Business Program es una experiencia de aprendizaje en línea innovadora traído por Kotler Impact Inc. en colaboración con Pearson Education. Estos programas e-learning certificados, son emitidos de forma exclusiva por la elite del mundo académico y el ámbito empresarial corporativo, incluyendo al gurú del marketing de renombre mundial, el Profesor Philip Kotler. Estos programas de capacitación están dirigidos para estudiantes y profesionales de diferentes niveles y habilidades en marketing, que quieren impulsar su potencial y alcanzar mayores éxitos sostenibles en los negocios.">
	<meta name="Keywords" content="Marketing, Curso marketing, curso marketing digital, curso web, curso online, curso kotler, kotler impact,  Educación, Capacitación, Aprendizaje, Conocimiento, Desarrollo, e-learning, e-books, Cursos, Programas, Kotler Impact, Pearson, Kotler Business Program, Innovación, Marketing Digital, Online, Académicos, Profesores, Estudiantes, Profesionales, Prestigio, Empresas, OTEC, OTIC, Sence, Habilidades, Éxito, Potencial, Philp Kotler, Al Ries, Rob Wolcott, Gestión empresarial, Líderes, Expertos, Estrategias, Empresarios, Emprendedores, Corporaciones, Mercados, Cliente, Posicionamiento, Comunicación, Certificados, Calidad, kotler chile, programa de negocios, bussines program kotler, educación online, rodrigo alvial, david contreras, estudiantes de marketing, profesores de marketing,  Marc Oliver Opresnik, kotler latam, kotler latinoamerica, cursos de kotler en latinoamerica, cursos kotler argentina, brasil, colombia, panama, ecuador ,perú, paraguay, uruguay, venezuela">
	<!-- load peace bar -->
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/pace.css">
	<script src="<?php bloginfo('template_directory'); ?>/assets/js/pace.js"></script>
	<!-- load Jquery -->
  <script src="<?php bloginfo('template_directory'); ?>/assets/js/jquery3.1.1.min.js"></script>
  <!--  bootstrap -->
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/bootstrap.min.css">
  <script src="<?php bloginfo('template_directory'); ?>/assets/js/bootstrap.min.js"></script>
  <!-- main y style.css -->
  <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
  <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/main.css">
	<!-- Load font/icons -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Oswald:300,400" rel="stylesheet">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!-- script varios -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/css/bootstrapValidator.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script>
</head>
<body>
<!-- Navbar -->
  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid menu">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-1">
          <span class="sr-only">Menu</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand">
          <img id="navkotler" class="logo" src="<?php bloginfo('template_directory'); ?>/assets/img/nav-kotler.jpg" alt="Kotler Impact">
          <img id="navpearson" class="logo" src="<?php bloginfo('template_directory'); ?>/assets/img/nav-pearson.jpg" alt="Pearson">
        </a>
        <a href="http://escueladeadministracion.uc.cl/educacion-ejecutiva/cursos/kotler-business-program/" class="navbar-brand">
          <img id="navuc" class="logo" src="<?php bloginfo('template_directory'); ?>/assets/img/nav-uc.jpg" alt="Universidad Catolica, Escuela de Administracion">
        </a>
      </div>
      <div class="collapse navbar-collapse" id="navbar-1">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="<?php echo site_url(); ?>">Home</a></li>
          <li><a href="<?php echo site_url(); ?>/nosotros">Nosotros</a></li>
          <li><a href="<?php echo site_url(); ?>/sobre-kbp">Sobre KBP</a></li>
          <li><a href="<?php echo site_url(); ?>/programa-de-estudio">Programas de estudio</a></li>
          <li><a href="<?php echo site_url(); ?>/profesores">Profesores</a></li>
          <li><a href="<?php echo site_url(); ?>/revista">Revista</a></li>
          <li><a href="<?php echo site_url(); ?>/alianza">Alianza UC</a></li>
          <li><a href="<?php echo site_url(); ?>/prensa">Prensa</a></li>
					<li><a class="entrada" href="https://portal.mypearson.com/login">Entrada</a></li>
          <li class="boton-comprar"><a href="<?php echo site_url(); ?>/adquirir-kbp" class="comprar"><span>Adquirir KBP</span></a></li>
        </ul>
      </div>
    </div>
    <div class="shadow"></div>
  </nav>
