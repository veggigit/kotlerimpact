<?php
// theme suport
add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );

function css_form_adquirir() {
  if(is_page(29)){
    wp_register_style('customscss', get_template_directory_uri() . '/assets/css/customs.css');
    wp_enqueue_style( 'customscss' );

    wp_register_script('customsjs', get_template_directory_uri() . '/assets/js/customs.js', '', '', true);
    wp_enqueue_script('customsjs');
  }
}
add_action('wp_enqueue_scripts', 'css_form_adquirir');
// remove p image
function filter_ptags_on_images($content){
    return preg_replace('/<p>\\s*?(<a .*?><img.*?><\\/a>|<img.*?>)?\\s*<\\/p>/s', '\1', $content);
}
add_filter('the_content', 'filter_ptags_on_images');
// [btn_link]
function btn_shortcode($atts, $content) {
  $atributo = shortcode_atts(array('link' => ''), $atts, 'btn_link');
  $btn = '<a href="'. $atts['link'] .'" class="btn btn-primary" style="margin-top:0;">';
  $btn .= $content;
  $btn .= '</a>';
  return $btn;
}
add_shortcode( 'btn_link', 'btn_shortcode' );
