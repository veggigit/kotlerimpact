  <footer id="footer">
    <div class="footer-brands">
      <div class="container-fluid">
        <!-- fila marcas -->
        <div class="row">
          <div class="col-xs-6 col-sm-4 col-md-2 text-center">
            <a href="http://www.kotlerbusinessprogram.com/"><img src="<?php bloginfo('template_directory'); ?>/assets/img/brand_foot1.jpg" class=""></a>
          </div>
          <div class="col-xs-6 col-sm-4 col-md-2 text-center">
            <a href="http://www.kotlerimpact.org/"><img src="<?php bloginfo('template_directory'); ?>/assets/img/brand_foot6.jpg" class=""></a>
          </div>
          <div class="col-xs-6 col-sm-4 col-md-2 text-center">
            <a href="http://www.kotlerawards.com/"><img src="<?php bloginfo('template_directory'); ?>/assets/img/brand_foot3.jpg" class=""></a>
          </div>
          <div class="col-xs-6 col-sm-4 col-md-2 text-center">
            <a href="http://www.worldmarketingsummitgroup.org/"><img src="<?php bloginfo('template_directory'); ?>/assets/img/brand_foot4.jpg" class=""></a>
          </div>
          <div class="col-xs-6 col-sm-4 col-md-2 text-center">
            <a href="http://www.kotlerimpact.org/"><img src="<?php bloginfo('template_directory'); ?>/assets/img/brand_foot5.jpg" class=""></a>
          </div>
          <div class="col-xs-6 col-sm-4  col-md-2 text-center">
            <a href="https://www.pearson.com/"><img src="<?php bloginfo('template_directory'); ?>/assets/img/brand_foot2.jpg" class=""></a>
          </div>
        </div>
        <!-- end fila marcas -->
      </div>
    </div>
    <div class="footer-datos">
      <div class="container-fluid">
        <!-- fila datos -->
        <div class="row">
          <div class="col-sm-6 col-md-6 text-left">
            <ul class="cierre-datos">
              <li class="primero">Oficina Chile: Callao 3037, Las Condes, Santiago</li>
              <li class="separador">+56(2) 23541232 / 9142</li>
              <li class="separador"><a href="mailto:gfogliatti@uc.cl">gfogliatti@uc.cl</a></li>
            </ul>
            <!-- <ul class="cierre-datos">
              <li class="primero"><a data-toggle="modal" data-target="#modalsoporte">Soporte</a></li>
              <li class="separador"><a data-toggle="modal" data-target="#modalterminos">Terminos de servicio</a></li>
            </ul> -->
            <span class="copy">
              Copyright Kotler Impact Chile 2018
            </span>
          </div>
          <div class="col-sm-6 col-md-6 text-right">
            <ul class="cierre-rrss">
              <li class="social"><a href="https://www.youtube.com/user/WorldMarketingSummit" target="_blank" class="fa fa-youtube" aria-hidden="true"></a></li>
              <li class="social"><a href="https://twitter.com/kotlerimpact" target="_blank" class="fa fa-twitter" aria-hidden="true"></a></li>
              <li class="social"><a href="https://www.facebook.com/kotlerbusinessprogramchile/?fref=ts" target="_blank" class="fa fa-facebook" aria-hidden="true"></a></li>
              <li class="social"><a href="https://www.linkedin.com/company/kotlerimpact-chile" target="_blank" class="fa fa-linkedin" aria-hidden="true"></a></li>
            </ul>
          </div>
        </div>
        <!-- end fila datos -->
      </div>
    </div>
  </footer>
  <?php wp_footer(); ?>
  <!-- modals -->
  <?php get_template_part( 'assets/include/content', 'modals' ); ?>
  <!-- sripts  -->
  <script src="<?php bloginfo('template_directory'); ?>/assets/js/tabs.js"></script>
  <script src="<?php bloginfo('template_directory'); ?>/assets/js/form-validation.js"></script>
  <script src="<?php bloginfo('template_directory'); ?>/assets/js/modals.js"></script>
</body>
</html>
