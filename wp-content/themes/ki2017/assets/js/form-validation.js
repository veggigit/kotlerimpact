$(document).ready(function(){
  var form = $('#formprogram');// validator iniciado con data-toggle http://1000hz.github.io/bootstrap-validator/
  form.validator().on('submit', function(e){
    if (e.isDefaultPrevented()) {
      console.log('Form: Houston, we have a problem');
    } else {
      console.log('Form: Send it');
      $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize(),
        success: function (data) {
          $("#modalsend").modal('show');// mostramos mensaje
          form[0].reset(); // resetiamos campos
        }
      });
      e.preventDefault();
    }

  });
});

$(function(){
  var formcompra = $('#formcompra');
  formcompra.validator().on('submit', function(e){
    if (e.isDefaultPrevented()){
      console.log('Favor completa el formulario');
    }else{
      console.log('Enviado');
    }

  })
})
