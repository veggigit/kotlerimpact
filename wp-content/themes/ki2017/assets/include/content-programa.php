<?php
?>
<div class="content-page">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="row">
          <div class="col-md-12">
            <ul id="tabs-programas" class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#im" aria-controls="im" role="tab" data-toggle="tab">Introducción al Marketing</a></li>
              <li role="presentation"><a href="#me" aria-controls="me" role="tab" data-toggle="tab">Marketing Esencial</a></li>
            </ul>
            <!-- tabs content  -->
            <div  class="tab-content">
              <div role="tabpanel" class="tab-pane fade in active" id="im">
                <h1 class="titulo-inside">Introducción al Marketing</h1>
                <h3>Descripción del programa</h3>
                <p>El objetivo de este programa es proporcionar a los estudiantes los conocimientos básicos y la comprensión de los principios fundamentales del marketing. Esta certificación consiste en un módulo de aprendizaje, a través de los contenidos entregados en una plataforma web, videos y lecturas adicionales</p>
                <h3>Objetivo de este programa</h3>
                <p>Este módulo proporciona una comprensión de los conceptos y terminología básicas y fundamentales utilizados en el marketing. Proporciona un conocimiento y comprensión del papel y la función del marketing dentro de las organizaciones y explora los factores que pueden influir en el comportamiento del consumidor. El módulo se describen los conceptos y elementos que componen el "marketing mix" y muestra la forma en que se aplican en el contexto.</p>
                <p>Los estudiantes aprenderán los desafíos y las oportunidades creadas por la naturaleza dinámica de los mercados estratégicos. También se estudiará estrategias de marketing, y determinar cuáles son relevantes y factibles para su organización y cuáles pueden maximizar las utilidades.</p>
                <h3>¿Quién puede beneficiarse de este programa</h3>
                <p>El programa está dirigido a alumnos de educación secundaria, estudiantes universitarios, empleados y jefaturas de mandos medios de empresas. Este programa también está dirigido para los estudiantes universitarios que no estudian carreras relacionadas con la economía o negocios (como la arquitectura, el diseño, etc.) y quieren adquirir nuevos conocimientos, construir su potencial y aumentar las oportunidades de trabajo posteriores. Por último, este programa está dirigido a los profesionales y empresarios que desean aplicar mejor las estrategias y herramientas de marketing para su trabajo diario.</p>
                <h3>Estructura del programa:</h3>
                <ol>
                  <li>La naturaleza de marketing: crear y capturar valor para el cliente.</li>
                  <li>La investigación de mercados y obtener comprensión del consumidor.</li>
                  <li>La creación de valor a través de los productos y marcas.</li>
                  <li>La creación de valor a través de precios.</li>
                  <li>La creación de valor a través de Distribución.</li>
                  <li>La creación de valor a través de Comunicación.</li>
                </ol>
                <h3>Requisitos de entrada:</h3>
                <p>No se requiere; Este programa está abierto a todos.</p>
                <h3>¿Cuánto tiempo tomará este programa?</h3>
                <ul>
                  <li>E-Learning (vídeo): 25 horas (recomendado, aunque los vídeos reales son de 6 horas de duración.)</li>
                  <li>E-books de aprendizaje: 45 horas.</li>
                  <li>Total E-Learning: 70 horas.</li>
                  <li>4 horas presenciales UC.</li>
                  <li>Prueba en línea: 2 horas.</li>
                </ul>
                <h3>Evaluaciones:</h3>
                <p>Las evaluaciones en línea están incluidas, las cuales se componen de preguntas de opción múltiple, así como ensayos y preguntas de discusión. Para obtener el certificado, el estudiante debe alcanzar una puntuación mínima de 50%, lo que se traduce en un grado "satisfactorio".</p>
                <h3>Certificado:</h3>
                <p>Al finalizar, los participantes recibirán un certificado que indique su participación y su respectivo rendimiento.</p>
              </div>
              <div role="tabpanel" class="tab-pane fade" id="me">
                <h1 class="titulo-inside">Marketing Esencial</h1>
                <h3>Descripción del programa</h3>
                <p>El objetivo de este programa es proporcionar a los estudiantes y profesionales los conocimientos esenciales y la comprensión de los principios fundamentales de la dirección de marketing. Esta certificación consiste en un módulo de aprendizaje, a través de los contenidos entregados en una plataforma web, videos y lecturas adicionales.</p>
                <h3>Objetivo del curso</h3>
                <p>Este programa proporciona una comprensión de los conceptos claves y la terminología utilizada en la gestión de marketing. También proporciona a los estudiantes un conocimiento y comprensión del papel y la función del marketing dentro de las organizaciones y explora los factores que pueden influir en el comportamiento del consumidor.</p>
                <p>Los estudiantes identificarán los componentes claves del entorno de marketing y desarrollarán una apreciación de cómo recoger y utilizar la información relevante. El módulo describen los conceptos y elementos que componen el "marketing mix" y muestra a los participantes cómo se aplican en el contexto. Los estudiantes aprenderán los desafíos y las oportunidades creadas por la naturaleza dinámica de los mercadosestratégicos. También se estudiará estrategias de marketing y a determinar cuáles son relevantes y factibles para su organización y cuáles pueden maximizar el resultado. Los estudiantes aprenderán las responsabilidades corporativas (incluyendo la verde y social), que pueden servir para dinamizar la organización y fomentar la cooperación y comunicación interna.</p>
                <p>Los participantes estudiarán propuestas de valor al cliente, activos y competencias, así como el análisis estratégico estructurado, incluyendo el detalle del cliente, competidor, mercado, ambiental y el análisis interno.</p>
                <h3>¿Quién puede beneficiarse de este programa?</h3>
                <p>El programa está dirigido para estudiantes universitarios y profesionales. Los estudiantes del programa se beneficiarán mediante la construcción de su potencial y aumentando las oportunidades de trabajo posteriores. Además, este programa está dirigido a profesionales y empresarios que desean aplicar mejor las estrategias y herramientas de marketing para su trabajo diario</p>
                <div class="foto-interior">
                  <img src="<?php bloginfo('template_directory'); ?>/assets/img/tablet-kotlerl.png" class="img-responsive" alt=>
                </div>
                <h3>Estructura del programa</h3>
                <p>Los Fundamentos de Marketing comprende la siguiente estructura y capítulos:</p>
                <ol>
                  <li>La naturaleza de marketing: crear y capturar valor para el cliente.</li>
                  <li>Estrategia de Empresa y Marketing.</li>
                  <li>Analizando el entorno del marketing.</li>
                  <li>La investigación de mercados y obtener comprensión del consumidor.</li>
                  <li>La comprensión del consumidor y comportamiento de la empresa compradora.</li>
                  <li>La segmentación de mercado, Targeting y Posicionamiento.</li>
                  <li>La creación de valor a través de los productos y marcas.</li>
                  <li>La creación de valor a través de precios.</li>
                  <li>La creación de valor a través de Distribución.</li>
                  <li>La creación de valor a través de Comunicación.</li>
                  <li>Marketing global</li>
                  <li>Marketing sostenible y Ética de Marketing</li>
                </ol>
                <h3>Requisitos</h3>
                <p>Este programa requiere un grado académico o experiencia profesional en Marketing.</p>
                <h3>¿Cuánto tiempo tomará este programa?</h3>
                <ul>
                  <li>E-Learning (vídeo): 50 horas (recomendado, aunque los vídeos reales son 10 horas de duración).</li>
                  <li>E-books de aprendizaje: 100 horas.</li>
                  <li>Total E-Learning: 150 horas.</li>
                  <li>4 horas presenciales UC.</li>
                  <li>Pruebas en línea: 3 horas.</li>
                </ul>
                <h3>Evaluaciones:</h3>
                <p>Las evaluaciones en línea están incluidos, que se compone de preguntas de opción múltiple, así como ensayos y preguntas de discusión. Para obtener el certificado, el estudiante debe alcanzar una puntuación mínima de 60%, lo que se traduce en un grado "satisfactorio".</p>
                <h3>Certificado:</h3>
                <p>Al finalizar, los participantes recibirán un certificado que indique su participación y su respectivo rendimiento.</p>
              </div>
            </div>
            <!-- end tabs content  -->
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="row">
          <div class="col-md-12 text-center">
              <a href="<?php echo site_url('/adquirir-kbp/') ?>" class="btn btn-primary">Adquiere tu KBP Hoy</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
