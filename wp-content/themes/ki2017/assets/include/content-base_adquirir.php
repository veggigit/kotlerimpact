<?php
   if ( is_user_logged_in() ) {
      ?>
      <div class="content-page">
        <div class="container">
          <div class="row">
            <div class="col-md-10 col-md-offset-1">
              <div class="row">
                <div class="col-md-12">
                  <h1 class="titulo-inside"><?php the_title(); ?></h1>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-10 col-md-offset-1">
              <div class="row">
                <div class="col-md-12">
                    <table class="table table-condensed">
                      <thead>
                        <tr>
                          <th>id</th>
                          <th>Nombre</th>
                          <th>Apelligdo</th>
                          <th>@</th>
                          <th>Dni</th>
                          <th>Nivel</th>
                          <th>Programa KBP</th>
                          <th>Fecha</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $contactos = $wpdb->get_results("SELECT * FROM wp_adquirir_mensajes");
                        foreach ( $contactos as $contacto)
                        {?>
                        <tr>
                          <td><?php echo $contacto->id; ?></td>
                          <td><?php echo $contacto->nombre; ?></td>
                          <td><?php echo $contacto->apellido; ?></td>
                          <td><?php echo $contacto->email; ?></td>
                          <td><?php echo $contacto->dni; ?></td>
                          <td><?php echo $contacto->nivel; ?></td>
                          <td><?php echo $contacto->programa; ?></td>
                          <td><?php echo $contacto->fecha; ?></td>
                        </tr>
                      <?php } ?>
                      </tbody>
                    </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
<?php }
else {?>
     <div class="content-page">
       <div class="container">
         <div class="row">
           <div class="col-md-10 col-md-offset-1">
             <div class="row">
               <div class="customs col-md-12 text-center">
               <?php $args = array('form_id' => 'log-db-adq');
               wp_login_form( $args );
               ?>
               </div>
             </div>
           </div>
         </div>
       </div>
     </div>
   <?php } ?>
