<?php
$title=apply_filters('the_title', get_post_field('post_title',17));
$content=apply_filters('the_content', get_post_field('post_content',17));
 ?>
 <div class="content-page">
   <div class="container">
     <div class="row">
       <div class="col-md-10 col-md-offset-1">
         <div class="row">
           <div class="col-md-12">
             <h1 class="titulo-inside"><?php echo $title ?></h1>
           </div>
         </div>
       </div>
     </div>
     <div class="row">
       <div class="col-md-10 col-md-offset-1">
         <div class="row">
           <div class="col-md-12">
             <?php echo $content ?>
               <ul class="noticias">
                 <li><div class="fecha">06/11/2017</div><a href="http://impresa.elmercurio.com/Pages/NewsDetail.aspx?dt=2017-11-06&dtB=06-11-2017%200:00:00&PaginaId=11&bodyid=2"><strong>El Mercurio</strong> <em>"Kotler Impact Chile apuesta por lo emprendedores"</em></a></li>
                 <li><div class="fecha">04/11/2017</div><a href="http://impresa.elmercurio.com/Pages/NewsDetail.aspx?dt=2017-11-04&PaginaId=12&BodyID=2"><strong>El Mercurio</strong> <em>"UC firmó alianza con Kotler Institute"</em></a></li>
                 <li><div class="fecha">18/10/2017</div><a href="http://amddchile.com/lanzamiento-alianza-escuela-de-administracion-uc-y-kotler-institute/"><strong>AMDDChile</strong> <em>"Lanzamiento Alianza Escuela de Administración UC Y Kotler Institute"</em></a></li>
                 <li><div class="fecha">18/10/2017</div><a href="http://www.duoc.cl/ver/noticia/duoc-uc-sede-antonio-varas-invita-comunidad-educativa-la-charla-de-marketing-40-segun-philip"><strong>Duoc UC</strong> <em>"Duoc UC sede antonio Varas invita a comunidad educativa a la charla de Marketing 4.0 según Philip Kotler"</em></a></li>
                 <li><div class="fecha">01/07/2016</div><a href="http://www.anda.cl/revista/Anda_Julio_2016/index.html#60"><strong>ANDA</strong> <em>"Programa Mundial Kotler Impact llegó a Chile"</em></a></li>
                 <li><div class="fecha">06/06/2016</div><a href="http://amddchile.com/noticias/lanzamiento-mundial-de-kotler-impact-se-realiza-en-chile/"><strong>Amdd</strong> <em>"Lanzamiento mundial de Kotler Impact se realiza en Chile"</em></a></li>
                 <li><div class="fecha">01/06/2016</div><a href="http://www.tecnopymes.cl/index.php/2016/06/01/kotler-lanza-curso-de-marketing-para-emprendedores/"><strong>Tecnopyme</strong> <em>"Kotler lanza curso de marketing para emprendedores"</em></a></li>
                 <li><div class="fecha">23/05/2016</div><a href="http://pyme.emol.com/3056/chile-curso-lideres-innovadores/"><strong>Emol</strong> <em>"Chile inaugurará inédito curso para líderes innovadores"</em></a></li>
                 <li><div class="fecha">17/05/2016</div><a href="https://cl.universianews.net/2016/05/17/lanzamiento-mundial-de-kotler-impact-se-realiza-en-chile/"><strong>Universia</strong> <em>"Lanzamiento mundial de Kotler Impact se realiza en Chile"</em></a></li>
                 <li><div class="fecha">16/05/2016</div><a href="https://www.youtube.com/watch?v=S5oXSLte8eY"><strong>Conferencia de prensa</strong> <em>"Hotel Intercontinental Santiago, Chile"</em></a></li>
               </ul>
           </div>
         </div>
       </div>
     </div>
     <div class="row">
       <div class="col-md-10 col-md-offset-1">
         <div class="row">
           <div class="col-md-12 text-center">
               <a href="<?php echo site_url('/adquirir-kbp/') ?>" class="btn btn-primary">¡Compra tu KPB Hoy!</a>
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>
