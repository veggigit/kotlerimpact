<?php
 ?>
 <script>
 Pace.on('done', function() {
   // if($('#banner').length){
   //   $('#modalvideo').modal('show');
   // }else{
   //   $('#modalvideo').modal('hide');
   // }
});
 </script>
 <!-- soporte -->
 <div id="modalsoporte" class="modal fade" tabindex="-1" role="dialog">
   <div class="modal-dialog modal-md" role="document">
     <div class="modal-content">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         <h5 class="modal-title" id="myModalLabel">Soporte técnico para estudiantes</h5>
       </div>
       <div class="modal-body">
         <div class="row">
           <div class="col-md-4">
             <div class="face">
               <img class="img-circle" src="<?php bloginfo('template_directory'); ?>/assets/img/soporte.jpg" width="170">
             </div>
           </div>
           <div class="col-md-8">
             <p>Si tienes alguna duda envianos un correo a: <a href="mailto:soporte@kotlerimpact.cl?subject=Soporte">soporte@kotlerimpact.cl</a></br> En breves momentos responderemos tu mensaje. </p>
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>
 <!-- terminos -->
 <div id="modalterminos" class="modal fade" tabindex="-1" role="dialog">
   <div class="modal-dialog modal-md" role="document">
     <div class="modal-content">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         <h5 class="modal-title" id="myModalLabel">Terminos de servicio</h5>
       </div>
       <div class="modal-body">
           <div class="row">
             <div class="col-md-12">
               <p>...</p>
             </div>
           </div>
       </div>
     </div>
   </div>
 </div>
 <!-- enviado -->
 <div id="modalsend" class="modal fade" tabindex="-1" role="dialog">
   <div class="modal-dialog modal-md" role="document">
     <div class="modal-content">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         <h5 class="modal-title" id="myModalLabel">Gracias</h5>
       </div>
       <div class="modal-body">
           <div class="row">
             <div class="col-md-12">
               <p>En breves momentos nos pondremos en contacto con ud.</p>
             </div>
           </div>
       </div>
     </div>
   </div>
 </div>
 <!-- video -->
 <div id="modalvideo" class="modal fade" tabindex="-1" role="dialog">
   <div class="modal-dialog modal-md" role="document">
     <div class="modal-content">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         <h5 class="modal-title" id="myModalLabel">Bienvenido a Kotler Business Program</h5>
       </div>
       <div class="modal-body">
       </div>
     </div>
   </div>
 </div>
