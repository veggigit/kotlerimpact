<?php
 ?>
 <section id="features">
   <div class="container">
     <div class="row">
       <div class="col-md-8 col-md-offset-2 text-center">
         <h1 class="titulo">El Método Perfecto</h1>
       </div>
     </div>
     <div class="row">
       <div class="col-md-3 text-center">
         <i class="material-icons icon-big">wifi</i>
         <h4>100% online</h4>
         <p>Nuestra moderna plataforma online te entrega total flexibilidad para estudiar en cualquier momento.</p>
       </div>
       <div class="col-md-3 text-center">
         <i class="material-icons icon-big">library_books</i>
         <h4>Ebooks de estudio</h4>
         <p>Aprendiendo directamente de los mejores del marketink moderno.</p>
       </div>
       <div class="col-md-3 text-center">
         <i class="material-icons icon-big">school</i>
         <h4>Feedback al instante</h4>
         <p>Nuestro sistema de evaluación periodica, permite realizar un seguimiento real de tu progresos. Estamos atentos a tus consultas, cuándo y dónde sea.</p>
       </div>
       <div class="col-md-3 text-center">
         <i class="material-icons icon-big">trending_up</i>
         <h4>Haz crecer tu negocio</h4>
         <p>Nuestro enfoque es totalmente práctico, esto significa que puedes aplicar las lecciones directamente a tu negocio.</p>
       </div>
     </div>
   </div>
 </section>
