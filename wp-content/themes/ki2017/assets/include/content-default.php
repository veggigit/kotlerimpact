<?php
$title=apply_filters('the_title', get_post_field('post_title'));
$content=apply_filters('the_content', get_post_field('post_content'));
 ?>
 <div class="content-page">
   <div class="container">
     <div class="row">
       <div class="col-md-10 col-md-offset-1">
         <div class="row">
           <div class="col-md-12">
             <h1 class="titulo-inside"><?php echo $title ?></h1>
           </div>
         </div>
       </div>
     </div>
     <div class="row">
       <div class="col-md-10 col-md-offset-1">
         <div class="row">
           <div class="col-md-12">
             <?php echo $content ?>
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>
