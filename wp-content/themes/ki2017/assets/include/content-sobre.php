<?php
// NOTE: WORPRESS, HOOK, APPLY_FILTERS, GET CONTENT, CONTENT BY ID. Con esta funcion traemos el contenido por id. a diferencia de get post field, aqui pasamo th_content para que el contenido se vea formateado html.
$title=apply_filters('the_title', get_post_field('post_title',11));
$content=apply_filters('the_content', get_post_field('post_content',11));
 ?>
 <div class="content-page">
   <div class="container">
     <div class="row">
       <div class="col-md-10 col-md-offset-1">
         <div class="row">
           <div class="col-md-12">
             <h1 class="titulo-inside"><?php echo $title ?></h1>
           </div>
         </div>
       </div>
     </div>
     <div class="row">
       <div class="col-md-10 col-md-offset-1">
         <div class="row">
           <div class="col-md-12">
             <?php echo $content ?>
             <div class="videoin">
               <div class="embed-responsive embed-responsive-16by9">
                 <iframe class="embed-responsive-item" src="//www.youtube.com/embed/x15jy4W_U1Q"></iframe>
               </div>
             </div>
           </div>
         </div>
       </div>
     </div>
     <div class="row">
       <div class="col-md-10 col-md-offset-1">
         <div class="row">
           <div class="col-md-12 text-center">
               <a href="#" class="btn btn-primary">Aprende marketing con los mejores</a>
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>
