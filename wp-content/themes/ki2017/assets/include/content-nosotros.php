<?php
$title=apply_filters('the_title', get_post_field('post_title', 2));
$content=apply_filters('the_content', get_post_field('post_content', 2));
 ?>
 <div class="content-page">
   <div class="container">
     <div class="row">
       <div class="col-md-10 col-md-offset-1">
         <div class="row">
           <div class="col-md-12">
             <h1 class="titulo-inside"><?php echo $title ?></h1>
           </div>
         </div>
       </div>
     </div>
     <div class="row">
       <div class="col-md-10 col-md-offset-1">
         <div class="row">
           <div class="col-md-12">
             <?php echo $content ?>
           </div>
         </div>
       </div>
     </div>
     <div class="row">
       <div class="col-md-10 col-md-offset-1">
         <div class="row">
           <div class="col-md-12 text-center">
               <a href="<?php echo site_url('/adquirir-kbp/') ?>" class="btn btn-primary">¡Aprende marketing hoy!</a>
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>
