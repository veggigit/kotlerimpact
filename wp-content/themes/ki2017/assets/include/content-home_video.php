<?php
 ?>
<header id="banner" class="media-container">
 <div class="container">
   <div class="row">
     <div class="col-sm-6 col-md-8">
       <h1 class="titulo-banner">Aprende Marketing</br><span>con los Mejores</span></h1>
       <a id="callvideo" href="#" class="playcontent video">
         <i class="material-icons play">play_circle_outline</i>
         <span class="slogan">Kotler Business Program, el prestigioso programa de entrenamiento en Marketing, ya está en Chile.</span>
       </a>
     </div>
     <div class="col-sm-6 col-md-4 text-center">
       <form role="form" data-toggle="validator" id="formprogram" action="<?php bloginfo('template_directory'); ?>/assets/form/envia.php" method="post" data-feedback='{"success": "fa-check", "error": "fa-times"}' >
         <div class="formheader">
           <span>
             <img class="img-responsive" src="<?php bloginfo('template_directory'); ?>/assets/img/logo_form.png" alt="Kotler business program">
           </span>
           <!-- <span class="pronto">Disponible desde Marzo 2017</span> -->
           <!-- <div class="dcto">
             <span>50</span>
           </div>
           <div class="datos-dctos">
             <span class="porcentaje">%</span>
             <span class="txt-dcto">dcto</span>
           </div>
           <span class="legal-dcto">Válido hasta el 31 de Dic 2017</span> -->
         </div>
         <div class="formbody">
           <div class="form-group has-feedback">
             <input type="text" class="form-control input-lg" name="name" id="name" pattern="[A-Za-z\s]{4,}" placeholder="Rodolfo Amunategui" data-error="Ay! Ingrese su nombre" required>
             <span class="fa form-control-feedback" aria-hidden="true"></span>
             <div class="help-block with-errors"></div>
           </div>
           <div class="form-group has-feedback">
             <input type="email" class="form-control input-lg" name="email" id="email" pattern="[a-zA-Z]{3,}@[a-zA-Z]{3,}[.]{1}[a-zA-Z]{2,}" placeholder="suemail@dominio.com" data-error="Oush! Revise su correo"  required>
             <span class="fa form-control-feedback" aria-hidden="true"></span>
             <div class="help-block with-errors"></div>
           </div>
           <div class="form-group has-feedback">
             <input type="tel" class="form-control input-lg" name="phone" id="phone" pattern="[0-9]{9}" placeholder="998856715" data-error="Ups! Campo requerido, min 9 digitos" required>
             <span class="fa form-control-feedback" aria-hidden="true"></span>
             <div class="help-block with-errors"></div>
           </div>
           <div class="form-group">
             <select class="form-control input-lg" name="eprogram" id="eprogram">
               <option>Introducción al Marketing (inglés/subtitulado)</option>
               <option>Marketing Esencial (inglés/subtitulado)</option>
             </select>
           </div>
           <button type="submit" class="btn verde">Infórmate ahora</button>
         </div>
       </form>
     </div>
   </div>
 </div>
 <span class="blackvid"></span>
 <video class="cover" width="960" height="540" autoplay loop>
   <source src="<?php bloginfo('template_directory'); ?>/assets/media/bg_video.mp4" type="video/mp4">
   </video>
 </header>
