<?php
$title=apply_filters('the_title', get_post_field('post_title',25));
$content=apply_filters('the_content', get_post_field('post_content',25));
 ?>
 <div class="content-page">
   <div class="container">
     <div class="row">
       <div class="col-md-10 col-md-offset-1">
         <div class="row">
           <div class="col-md-12">
             <h1 class="titulo-inside"><?php echo $title ?></h1>
           </div>
         </div>
       </div>
     </div>
     <div class="row">
       <div class="col-md-10 col-md-offset-1">
         <div class="row">
           <div class="col-md-12">
             <?php echo $content ?>   
                 <ul class="noticias">
                   <li><div class="fecha">MYM 2016</div><a href="http://online.flipbuilder.com/mech/ozmd/mobile/index.html"><strong>Mind your Marketing 2016</strong></a></li>
                   <li><div class="fecha">MYM 2015</div><a href="http://online.flipbuilder.com/mech/nbal/mobile/index.html"><strong>Mind your Marketing 2015</strong></a></li>
                 </ul>
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>
