<?php
 ?>
<section id="program">
 <div class="container">
   <div class="row">
     <div class="col-md-12 text-center">
       <h1 class="titulo">Programas de Estudio</h1>
    </div>
   </div>
   <div class="row">
     <div class="col-md-6 text-left">
       <h3>Introducción al Marketing</h3>
       <p>El programa está dirigido a alumnos de educación secundaria y universitaria, empleados y jefaturas que quieren adquirir nuevos conocimientos y aplicar mejor las estrategias del marketing a su trabajo y/o emprendimiento.</p>
       <!-- <div class="table-program">
         <div class="table-header flex">
           <div class="titulo-table">
             <i class="material-icons">school</i>
             <h4>Marketing Esencial</h4>
           </div>
         </div>
         <div class="table-content">
           <ul class="items">
             <li>
               <p class="hrs">35<span>hrs</span></p>
               <p class="mat">E-learning video</p>
             </li>
             <li>
               <p class="hrs">100<span>hrs</span></p>
               <p class="mat">Aprendizaje por E-book</p>
             </li>
             <li>
               <p class="hrs">6<span>hrs</span></p>
               <p class="mat">Casos negocios</p>
             </li>
             <li>
               <p class="hrs">6<span>hrs</span></p>
               <p class="mat">Proyecto de negocios</p>
             </li>
             <li>
               <p class="hrs">3<span>hrs</span></p>
               <p class="mat">Evaluaciones</p>
             </li>
           </ul>
         </div>
       </div> -->
     </div>
     <div class="col-md-6 text-left">
       <h3>Marketing Esencial</h3>
       <p>El programa está dirigido a estudiantes, profesionales y emprendedores que tengan al menos un grado académico. Los estudiantes aprenderán las nuevas estrategias del marketing moderno, por medio de catedráticos y líderes corporativos de renombre mundial.</p>
     </div>
   </div>
   <div class="row">
     <div class="col-md-12 text-center">
       <a href="<?php echo site_url(); ?>/programa-de-estudio" class="btn aqua">saber más</a>
     </div>
   </div>
 </div>
</section>
