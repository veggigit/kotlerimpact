<section id="profesores-destacados">
  <div class="container">
    <div class="row">
      <div class="col-md-3 text-left">
        <h1 class="titulo">Profesores</h1>
        <p>Todos nuestros profesores provienen de las mejores universidades del mundo y de empresas multinacionales de renombre mundial.</p>
        <p>Hemos puesto sus conocimientos y experiencias a tu alcance.</p>
        <a class="btn blue" href="<?php echo site_url(); ?>/profesores">saber más</a>
      </div>
      <div class="col-md-3 text-center">
        <div class="profile">
          <div class="face">
            <img class="img-circle" src="http://test.kotlerbusinessprogram.com/wp-content/uploads/2016/04/Kotler240.jpg" width="170">
          </div>
          <div class="name">
            <h4>Philip Kotler</h4>
          </div>
          <div class="curriculum">
            <p>Founder, Kotler Business Program Distinguished Marketing Professor, Kellogg School of Management</p>
          </div>
        </div>
      </div>
      <div class="col-md-3 text-center">
        <div class="profile">
          <div class="face">
            <img class="img-circle" src="http://test.kotlerbusinessprogram.com/wp-content/uploads/2016/05/Takaoka240.jpg" width="170">
          </div>
          <div class="name">
            <h4>Kozo Takaoka</h4>
          </div>
          <div class="curriculum">
            <p>President, Kotler Business Program President and CEO, Nestlé Japan</p>
          </div>
        </div>
      </div>
      <div class="col-md-3 text-center">
        <div class="profile">
          <div class="face">
            <img class="img-circle" src="http://test.kotlerbusinessprogram.com/wp-content/uploads/2016/05/Opresnik240-1.jpg" width="170">
          </div>
          <div class="name">
            <h4>Marc Oliver Opresnik</h4>
          </div>
          <div class="curriculum">
            <p>CEO, Kotler Business Program Marketing Professor, Luebeck University of Applied Sciences, Germany</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
