<?php
$title=apply_filters('the_title', get_post_field('post_title',15));
$content=apply_filters('the_content', get_post_field('post_content',15));
?>
<div class="content-page">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="row">
          <div class="col-md-12">
            <h1 class="titulo-inside"><?php echo $title ?></h1>
            <?php echo $content ?>
          </div>
        </div>
      </div>
    </div>
    <!-- Fila profesores destacados -->
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="row">
          <div class="panel">
            <div class="panel-body">
              <div class="col-md-4 text-center">
                <div class="profile">
                  <div class="face">
                    <img class="img-circle" src="http://test.kotlerbusinessprogram.com/wp-content/uploads/2016/04/Kotler240.jpg" width="170">
                  </div>
                  <div class="name">
                    <h4 class="azul">Philip Kotler</h4>
                  </div>
                  <div class="curriculum">
                    <p>Founder, Kotler Business Program Distinguished Marketing Professor,Kellogg School of Management</p>
                  </div>
                </div>
              </div>
              <div class="col-md-4 text-center">
                <div class="profile">
                  <div class="face">
                    <img class="img-circle" src="http://test.kotlerbusinessprogram.com/wp-content/uploads/2016/05/Takaoka240.jpg" width="170">
                  </div>
                  <div class="name">
                    <h4 class="azul">Kozo Takaoka</h4>
                  </div>
                  <div class="curriculum">
                    <p>President, Kotler Business Program President and CEO, Nestlé Japan</p>
                  </div>
                </div>
              </div>
              <div class="col-md-4 text-center">
                <div class="profile">
                  <div class="face">
                    <img class="img-circle" src="http://test.kotlerbusinessprogram.com/wp-content/uploads/2016/05/Opresnik240-1.jpg" width="170">
                  </div>
                  <div class="name">
                    <h4 class="azul">Marc Oliver Opresnik</h4>
                  </div>
                  <div class="curriculum">
                    <p>CEO, Kotler Business Program Marketing Professor, Luebeck University of Applied Sciences, Germany</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Fila profesores -->
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="row">
          <div class="col-md-4 text-center">
            <div class="profile">
              <div class="face">
                <img class="img-circle" src="http://test.kotlerbusinessprogram.com/wp-content/uploads/2016/05/baalbaki240.jpg" width="170">
              </div>
              <div class="name">
                <h4 class="azul">Imad Balbaki</h4>
              </div>
              <div class="curriculum">
                <p>VP of Development, American University of Beirut</p>
              </div>
            </div>
          </div>
          <div class="col-md-4 text-center">
            <div class="profile">
              <div class="face">
                <img class="img-circle" src="http://test.kotlerbusinessprogram.com/wp-content/uploads/2016/05/brown240.jpg" width="170">
              </div>
              <div class="name">
                <h4 class="azul">Linden Brown</h4>
              </div>
              <div class="curriculum">
                <p>Chairman, MarketCulture</p>
              </div>
            </div>
          </div>
          <div class="col-md-4 text-center">
            <div class="profile">
              <div class="face">
                <img class="img-circle" src="http://test.kotlerbusinessprogram.com/wp-content/uploads/2016/05/french240.jpg" width="170">
              </div>
              <div class="name">
                <h4 class="azul">Jeff French</h4>
              </div>
              <div class="curriculum">
                <p>CEO, Strategic Social Marketing</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Fila profesores -->
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="row">
          <div class="col-md-4 text-center">
            <div class="profile">
              <div class="face">
                <img class="img-circle" src="http://test.kotlerbusinessprogram.com/wp-content/uploads/2016/05/gummeson240.jpg" width="170">
              </div>
              <div class="name">
                <h4 class="azul">Evert Gummesson</h4>
              </div>
              <div class="curriculum">
                <p>Professor Emeritus, Stockholm Business School</p>
              </div>
            </div>
          </div>
          <div class="col-md-4 text-center">
            <div class="profile">
              <div class="face">
                <img class="img-circle" src="http://test.kotlerbusinessprogram.com/wp-content/uploads/2016/05/Hanssens240.jpg" width="170">
              </div>
              <div class="name">
                <h4 class="azul">Dominique Hanssens</h4>
              </div>
              <div class="curriculum">
                <p>Distinguished Marketing Professor, UCLA School of Management</p>
              </div>
            </div>
          </div>
          <div class="col-md-4 text-center">
            <div class="profile">
              <div class="face">
                <img class="img-circle" src="http://test.kotlerbusinessprogram.com/wp-content/uploads/2016/05/horton240.jpg" width="170">
              </div>
              <div class="name">
                <h4 class="azul">Chris Horton</h4>
              </div>
              <div class="curriculum">
                <p>Digital Strategist, Synecore</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Fila profesores -->
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="row">
          <div class="col-md-4 text-center">
            <div class="profile">
              <div class="face">
                <img class="img-circle" src="http://test.kotlerbusinessprogram.com/wp-content/uploads/2016/05/kartajaya.jpg" width="170">
              </div>
              <div class="name">
                <h4 class="azul">Hermawan Kartajaya</h4>
              </div>
              <div class="curriculum">
                <p>Founder & CEO, MarkPlus Inc.</p>
              </div>
            </div>
          </div>
          <div class="col-md-4 text-center">
            <div class="profile">
              <div class="face">
                <img class="img-circle" src="http://test.kotlerbusinessprogram.com/wp-content/uploads/2016/05/Kaufman.jpg" width="170">
              </div>
              <div class="name">
                <h4 class="azul">Ira Kaufman</h4>
              </div>
              <div class="curriculum">
                <p>CEO,Entwine Digital.</p>
              </div>
            </div>
          </div>
          <div class="col-md-4 text-center">
            <div class="profile">
              <div class="face">
                <img class="img-circle" src="http://test.kotlerbusinessprogram.com/wp-content/uploads/2016/05/kuczmarski240.jpg" width="170">
              </div>
              <div class="name">
                <h4 class="azul">Tom Kuczmarski</h4>
              </div>
              <div class="curriculum">
                <p>Senior Partner & President, Kuczmarski Innovation.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Fila profesores -->
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="row">
          <div class="col-md-4 text-center">
            <div class="profile">
              <div class="face">
                <img class="img-circle" src="http://test.kotlerbusinessprogram.com/wp-content/uploads/2016/05/lee240.jpg" width="170">
              </div>
              <div class="name">
                <h4 class="azul">Nancy Lee</h4>
              </div>
              <div class="curriculum">
                <p>Founder & President, Social Marketing Services, Inc.</p>
              </div>
            </div>
          </div>
          <div class="col-md-4 text-center">
            <div class="profile">
              <div class="face">
                <img class="img-circle" src="http://test.kotlerbusinessprogram.com/wp-content/uploads/2016/05/Lindstrom240.jpg" width="170">
              </div>
              <div class="name">
                <h4 class="azul">Martin Lindstrom</h4>
              </div>
              <div class="curriculum">
                <p>Branding Expert & Consultant.</p>
              </div>
            </div>
          </div>
          <div class="col-md-4 text-center">
            <div class="profile">
              <div class="face">
                <img class="img-circle" src="http://test.kotlerbusinessprogram.com/wp-content/uploads/2016/05/mann240.jpg" width="170">
              </div>
              <div class="name">
                <h4 class="azul">Igor Mann</h4>
              </div>
              <div class="curriculum">
                <p>Founder, Kongru</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Fila profesores -->
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="row">
          <div class="col-md-4 text-center">
            <div class="profile">
              <div class="face">
                <img class="img-circle" src="http://test.kotlerbusinessprogram.com/wp-content/uploads/2016/05/ries240.jpg" width="170">
              </div>
              <div class="name">
                <h4 class="azul">Al Ries</h4>
              </div>
              <div class="curriculum">
                <p>Co-Founder & Chairman, Ries & Ries Focusing Consultants.</p>
              </div>
            </div>
          </div>
          <div class="col-md-4 text-center">
            <div class="profile">
              <div class="face">
                <img class="img-circle" src="http://test.kotlerbusinessprogram.com/wp-content/uploads/2016/05/rogers240.jpg" width="170">
              </div>
              <div class="name">
                <h4 class="azul">Martha Rogers</h4>
              </div>
              <div class="curriculum">
                <p>Founding Partner,Peppers & Rogers Group.</p>
              </div>
            </div>
          </div>
          <div class="col-md-4 text-center">
            <div class="profile">
              <div class="face">
                <img class="img-circle" src="http://test.kotlerbusinessprogram.com/wp-content/uploads/2016/05/sawnhey240.jpg" width="170">
              </div>
              <div class="name">
                <h4 class="azul">Mohan Sawhney</h4>
              </div>
              <div class="curriculum">
                <p>McCormick Tribune Professor of Technology, Kellogg School of Management.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Fila profesores -->
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="row">
          <div class="col-md-4 text-center">
            <div class="profile">
              <div class="face">
                <img class="img-circle" src="http://test.kotlerbusinessprogram.com/wp-content/uploads/2016/05/seth240.jpg" width="170">
              </div>
              <div class="name">
                <h4 class="azul">Jagdish Seth</h4>
              </div>
              <div class="curriculum">
                <p>Marketing Professor,Emory University Business School.</p>
              </div>
            </div>
          </div>
          <div class="col-md-4 text-center">
            <div class="profile">
              <div class="face">
                <img class="img-circle" src="http://test.kotlerbusinessprogram.com/wp-content/uploads/2016/05/simon240.jpg" width="170">
              </div>
              <div class="name">
                <h4 class="azul">Hermann Simon</h4>
              </div>
              <div class="curriculum">
                <p>Founder & Chairman,Simon-Kucher & Partner.</p>
              </div>
            </div>
          </div>
          <div class="col-md-4 text-center">
            <div class="profile">
              <div class="face">
                <img class="img-circle" src="http://test.kotlerbusinessprogram.com/wp-content/uploads/2016/05/vieira240.jpg" width="170">
              </div>
              <div class="name">
                <h4 class="azul">Walter Vieira</h4>
              </div>
              <div class="curriculum">
                <p>Marketing Consultant.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Fila profesores -->
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="row">
          <div class="col-md-4 text-center">
            <div class="profile">
              <div class="face">
                <img class="img-circle" src="http://test.kotlerbusinessprogram.com/wp-content/uploads/2016/05/wolcott240.jpg" width="170">
              </div>
              <div class="name">
                <h4 class="azul">Rob Wolcott</h4>
              </div>
              <div class="curriculum">
                <p>Co-Founder & Executive Director, Kellogg Innovation Network (KIN).</p>
              </div>
            </div>
          </div>

          <div class="col-md-4 text-center">
            <div class="profile">
              <div class="face">
                <img class="img-circle" src="<?php bloginfo('template_url') ?>/assets/img/profesores/andres_ibanez.jpg" width="170">
              </div>
              <div class="name">
                <h4 class="azul">Andres Ibañez</h4>
              </div>
              <div class="curriculum">
                <p>Director Centro de Desarrollo.</br> Directivo y Director Relaciones Internacionales de la Escuela de Administración UC.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="row">
          <div class="col-md-12 text-center">
              <a href="<?php echo site_url('/adquirir-kbp/') ?>" class="btn btn-primary">Aprende marketing con los mejores</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
