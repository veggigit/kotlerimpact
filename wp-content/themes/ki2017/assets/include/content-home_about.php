<?php
?>
<section id="about">
 <div class="container">
   <div class="row">
     <div class="col-md-6 text-center">
       <div class="face">
         <img class="img-circle" src="<?php bloginfo('template_url') ?>/assets/img/kotler-home.jpg" width="60%">
       </div>
     </div>
     <div class="col-md-6">
       <h1 class="titulo">Sobre KBP</h1>
       <p>Kotler Business Program es una experiencia de aprendizaje en línea innovadora traído por Kotler Impact Inc. en colaboración con Pearson Education. Estos programas e-learning certificados, son emitidos de forma exclusiva por la elite del mundo académico y el ámbito empresarial corporativo, incluyendo al gurú del marketing de renombre mundial, el Profesor Philip Kotler.</p>
       <p>Estos programas de capacitación están dirigidos para estudiantes y profesionales de diferentes niveles y habilidades en marketing, que quieren impulsar su potencial y alcanzar mayores éxitos sostenibles en los negocios.</p>
       <p>Nuestro innovador enfoque de aprendizaje combinado consta de videos exclusivos de los principales expertos, eBooks de auto-estudio, simulaciones de vídeo, juegos de rol y evaluaciones, asegurando una experiencia de aprendizaje y prácticas de primer nivel.</p>
       <a href="<?php echo site_url(); ?>/sobre-kbp" class="btn blue">saber más</a>
     </div>
   </div>
 </div>
</section>
