<?php
$title=apply_filters('the_title', get_post_field('post_title', 19));
$content=apply_filters('the_content', get_post_field('post_content', 19));
 ?>
<div class="content-page">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="row">
          <div class="col-md-12">
            <h1 class="titulo-inside"><?php echo $title ?></h1>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="row">
          <div class="col-md-12">
            <?php echo $content ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 text-center">
            <i class="material-icons icon-big">dvr</i>
            <h4 class="azul">1_Formulario</h4>
            <p>Completar la información requerida del alumno.</p>
          </div>
          <div class="col-md-4 text-center">
            <i class="material-icons icon-big">shopping_cart</i>
            <h4 class="azul">2_Comprar</h4>
            <p>Realiza la transferencia electrónica.</p>
          </div>
          <div class="col-md-4 text-center">
            <i class="material-icons icon-big">mail_outline</i>
            <h4 class="azul">3_Acceso a KBP</h4>
            <p>Recibirás a un email con el acceso a la plataforma de estudio Pearson.</p>
          </div>
        </div>
      </div>
    </div>
    <div class="row compra-programas">
      <div class="col-md-10 col-md-offset-1">
        <div class="row">
          <div class="col-md-6 text-center">
            <!-- panel plan -->
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title">KBP Introducción al Marketing</h3>
              </div>
              <div class="panel-body">
                <!-- <div class="the-price">
                  <h1>$299.990<span class="subscript">clp</span></h1>
                  <small>Válido hasta 27 Sept.</small>
                </div> -->
                <table class="table">
                  <tr>
                    <td>
                      E-Learning (vídeo): 25 horas.
                    </td>
                  </tr>
                  <tr class="active">
                    <td>
                      E-books de aprendizaje: 45 horas
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Total E-Learning: 70 horas
                    </td>
                  </tr>
                  <tr class="active">
                    <td>
                      Prueba en línea: 2 horas
                    </td>
                  </tr>
                  <tr>
                    <td>
                      4 horas presenciales UC
                    </td>
                  </tr>
                  <tr class="active">
                    <td>
                      Certificado al aprobar el programa
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <a class="btn blue" href="<?php echo $siteUrl; ?>/programa-de-estudio">Info programa</a>
                    </td>
                  </tr>
                </table>
              </div>
              <!-- <div class="panel-footer">

              </div> -->
            </div>
            <!-- end plan -->
          </div>
          <div class="col-md-6 text-center">
            <!-- panel plan -->
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title">KBP Marketing Esencial</h3>
              </div>
              <div class="panel-body">

                <table class="table">
                  <tr>
                    <td>
                      E-Learning (vídeo): 50 horas
                    </td>
                  </tr>
                  <tr class="active">
                    <td>
                      E-books de aprendizaje: 100 horas
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Total E-Learning: 150 horas
                    </td>
                  </tr>
                  <tr class="active">
                    <td>
                      Pruebas en línea: 3 horas
                    </td>
                  </tr>
                  <tr>
                    <td>
                      4 horas presenciales UC
                    </td>
                  </tr>
                  <tr class="active">
                    <td>
                      Certificado KBP
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <a class="btn blue" href="<?php echo $siteUrl; ?>/programa-de-estudio">Info programa</a>
                    </td>
                  </tr>
                </table>
              </div>
              <!-- <div class="panel-footer">

              </div> -->
            </div>
            <!-- end plan -->
          </div>
        </div>
      </div>
    </div>

<?php if(!isset($_POST['submit'])){
  ?>
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="row">
          <div class="col-md-12">
            <h3>Formulario de compra</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">
            <form id="formcompra" method="POST" action="">

            <div class="row">
              <div class="col-md-12 text-left">
                <div class="form-group">
                  <label  for="nombre">Nombre</label>
                  <input type="text" pattern="[A-Za-z]{3,20}" class="form-control" id="nombre" name="nombre" placeholder="Nombre" data-error="Ingrese nombre" required>
                  <div class="help-block with-errors"></div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12 text-left">
                <div class="form-group">
                  <label  for="nombre">Apellido</label>
                  <input type="text" pattern="[A-Za-z]{3,20}" class="form-control" id="apellido" name="apellido" placeholder="Apellido" data-error="Ingrese apellido" required>
                  <div class="help-block with-errors"></div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12 text-left">
                <div class="form-group">
                  <label  for="nombre">Email</label>
                  <input type="email" class="form-control" id="email" name="email" placeholder="Email" data-error="Ingrese mail" required>
                  <div class="help-block with-errors"></div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12 text-left">
                <div class="form-group">
                  <label  for="nombre">Rut/dni</label>
                  <input type="text" pattern="[0-9]{8,9}" class="form-control" id="dni" name="dni" placeholder="163581886" data-error="Ingrese rut válido. Sin guion ni puntos" required>
                  <div class="help-block with-errors"></div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12 text-left">
                <div class="form-group">
                  <label for="nivel">Nivel de estudios</label>
                  <select class="form-control" id="nivel" name="nivel">
                    <option value="escolar">Escolar</option>
                    <option value="tecnico">Técnico</option>
                    <option value="universitario">Universitario</option>
                    <option value="postgrado">Postgrado</option>
                  </select>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12 text-left">
                <div class="form-group">
                  <label for="programa">Programa</label>
                  <select class="form-control" id="programa" name="programa">
                    <option value="intro">Introducción al Marketing (inglés/subtitulado)</option>
                    <option value="esencial">Marketing Esencial (inglés/subtitulado)</option>
                  </select>
                </div>
              </div>
            </div>

            <div class="row form-group">
              <div class="col-md-12 text-center">
                <button type="submit" name="submit" class="btn verde" value="1">adquirir</button>
              </div>
            </div>

            </form>
          </div>
        </div>
      </div>
    </div>
    <?php
  } else{
    // NOTE: WORDPRESS, DATABASE, DB, INSERT, CONEXION, BASE DE DATOS. Con esto ingresamos datos a la base.

    // ruta de llaves a la base de datos
    require_once('wp-config.php');
    //globalizamos clase
    global $wpdb;
    // conectamos y usamos llaves del wp-config.php --> sólo si usamos las llaves de nuestra db wp
    $conexion = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD);
    // check conexion
    if(!$conexion){
      echo("<script>console.log('Matrix: NO CONECTADO');</script>");
    }else{
      echo("<script>console.log('Matrix: CONECTADO');</script>");
    }
    // guardamos datos de input en variables
    $nombre = $_POST['nombre'];
    $apellido = $_POST['apellido'];
    $email = $_POST['email'];
    $dni = $_POST['dni'];
    $nivel = $_POST['nivel'];
    $programa = $_POST['programa'];
    // insertamos datos
    $wpdb->insert( 'wp_adquirir_mensajes', array(
      'nombre' => $nombre,
      'apellido' => $apellido,
      'email' => $email,
      'dni' => $dni,
      'programa' => $programa,
      'nivel' => $nivel
      // esto es para valida %s = string %d = digitos
    ), array(
      '%s',
      '%s',
      '%s',
      '%d',
      '%s',
      '%s'
    )
  );
}
  ?>
  </div>
</div>

<?php /*
if(isset($_POST['submit'])){
  global $wpdb;
  $nombre = $_POST['nombre'];
  $wpdb->insert( 'wp_adquirir_mensajes', array( 'nombre' => 55555 ), array('%s') );
}

function enviar (){
    global $wpdb;
    $nombre = $_POST['nombre'];
    $wpdb->insert( 'wp_adquirir_mensajes', array( 'nombre' => $nombre ), array('%s') );
}
enviar();

capturamos data
$nombre = $_POST['nombre'];
$pellido = $_POST['apellido'];
$rut = $_POST['rut'];
$direccion = $_POST['direccion'];
$pais = $_POST['pais'];
$cod = $_POST['cod'];
$tel = $_POST['tel'];
$email = $_POST['email'];
$nivel = $_POST['nivel'];
$programa = $_POST['programa'];

$table_name = "wp_adquirir_mensajes";
$wpdb->insert( $table_name, array(
    'nombre' => $_POST['nombre']
    'apellido' => $_POST['apellido']
    'rut' => $rut,
    'direccion' => $direccion,
    'pais' => $pais,
    'cod' => $cod,
    'tel' => $tel,
    'email' => $eamil,
    'nivel' => $nivel,
    'programa' => $programa,


$insertar = "INSERT INTO wp_adquirir_mensajes (nombre, apellido, rut, direccion, pais, cod, tel, email, nivel, programa) VALUES('$nombre','$apellido','$rut','$direccion','$pais','$cod','$tel','$email','$nivel','$programa')";
$listo = mysqli_query($conexion, $insertar);
if(!$listo){
  echo "no se ingreso";
}else{
  echo "estamos ready";
}
?>
